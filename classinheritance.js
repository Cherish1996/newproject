class Student{
    constructor(name,age,address,rollNumber){
        this.name=name
        this.age=age
        this.address=address
        this.rollNumber=rollNumber

    }
    displayStudent(){
        return `name:${this.name},age:${this.age},address:${this.address},rollNumber:${this.rollNumber}`
    }

}
class Player extends Student{
    constructor(name,age,address,rollNumber,game){
        super(name,age,address,rollNumber)
        this.game=game

    }
    displayPlayer(){
        return `name:${this.name},age:${this.age},address:${this.address},rollNumber:${this.rollNumber},game:${this.game}`
    }
}
let result=new Player('jackson','24','dhankuta','12','football')
document.getElementById('result').innerHTML=result.displayStudent()
document.getElementById('result2').innerHTML=result.displayPlayer()
